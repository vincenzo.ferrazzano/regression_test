#pragma once
#include <cassert>
#include <fstream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>

inline std::vector<std::string> split(const std::string& s, char delim) {
		std::vector<std::string> result;
		std::stringstream ss(s);
		std::string item;

		while (getline(ss, item, delim)) {
			result.push_back(item);
		}

		return result;
	}

	//Forward declaration
	template<typename eT>
	class ColumnVector;

	/**
	 * \brief A small class that contains a column-major array. This is meant to be a easy-to-navigate container for memory location to be fed to BLAS/LAPACK
	 * \tparam eT the type of the data contained
	 */
	template<typename eT>
	class ColumnMajorMatrix
	{
	public:
		ColumnMajorMatrix(unsigned long long n_rows, unsigned long long n_cols = 1) :
			n_rows{ n_rows },
			n_cols{ n_cols },
			memory_{ std::make_unique<eT[]>(n_rows * n_cols) }
		{
		}

		ColumnMajorMatrix(ColumnMajorMatrix&&) = default;
		ColumnMajorMatrix& operator=(ColumnMajorMatrix&&) = default;

		ColumnMajorMatrix& operator=(const ColumnMajorMatrix& other)
		{
			*this(other);
			return *this;
		}

		ColumnMajorMatrix(const ColumnMajorMatrix& other) : ColumnMajorMatrix(other.n_rows, other.n_cols)
		{
			std::memcpy(memory_.get(), other.get_memptr(), sizeof(eT) * other.n_elements());
		}


		const unsigned long long n_rows;
		const unsigned long long n_cols;

		eT const* get_col_ptr(unsigned long long col_number) const
		{
			if (col_number >= n_cols)
			{
				throw std::runtime_error("Out of bounds!");
			}
			const auto beginning_col_pos = col_number * n_rows;
			return memory_.get() + beginning_col_pos;
		}

		ColumnVector<eT> getCol(unsigned long long column) const
		{
			if (column >= n_cols)
			{
				throw std::runtime_error("Out of bounds!");
			}
			ColumnVector<eT> res(this->n_rows);
			for (unsigned i = 0; i < this->n_rows; i++)
			{
				res(i) = this->operator()(i, column);
			}
			return res;
		}

		eT* get_col_ptr(unsigned long long col_number)
		{
			if (col_number >= n_cols)
			{
				throw std::runtime_error("Out of bounds!");
			}
			const auto beginning_col_pos = col_number * n_rows;
			return memory_.get() + beginning_col_pos;
		}

		eT const* get_memptr() const
		{
			return get_col_ptr(0);
		}

		eT* get_memptr()
		{
			return get_col_ptr(0);
		}

		unsigned long long n_elements() const
		{
			return this->n_rows * this->n_cols;
		}

		const eT& operator()(unsigned long long row) const
		{
			return (*this)(row, 0);
		}

		const eT& operator()(unsigned long long row, unsigned long long col) const
		{
			auto const flat_pos = computePositionInMemory_(row, col);
			return this->memory_.get()[flat_pos];
		}

		eT& operator()(unsigned long long row)
		{
			return (*this)(row, 0);
		}

		eT& operator()(unsigned long long row, unsigned long long col)
		{
			auto const flat_pos = computePositionInMemory_(row, col);
			return this->memory_.get()[flat_pos];
		}

		template<typename eT2 = eT>
		ColumnMajorMatrix<eT2> clone() const
		{
			auto result = ColumnMajorMatrix<eT2>(this->n_rows, this->n_cols);
			if constexpr (std::is_same_v<eT, eT2>) {
				memcpy(result.memory_.get(), this->memory_.get(), this->n_elements() * sizeof(eT));
			}
			else
			{
				for (unsigned long long p = 0; p < this->n_elements(); p++)
				{
					result.get_memptr()[p] = static_cast<eT2>(this->memory_.get()[p]);
				}
			}
			return result;
		}

		double get_allocated_memory_mb() const
		{
			return this->n_elements() * sizeof(eT) / 1.0e6;
		}

		friend std::ostream& operator<<(
			std::ostream& os				/**< Stream*/,
			const ColumnMajorMatrix& matrix	/**< Matrix*/
			)
		{
			matrix.show(os, 0, 0);
			return os;
		}

		void show(unsigned long long row_to_show = 0, unsigned long long col_to_show = 0) const
		{
			show(std::cout, row_to_show, col_to_show);
		}

		void show(std::ostream& os, unsigned long long row_to_show=0, unsigned long long col_to_show=0) const
		{
			unsigned long long row_to_show_ = this->n_rows;
			unsigned long long col_to_show_ = this->n_cols;

			if (row_to_show > 0)
			{
				row_to_show_ = std::min(this->n_rows, row_to_show);
			}

			if (col_to_show > 0)
			{
				col_to_show_ = std::min(this->n_cols, col_to_show);
			}

			unsigned long long r;
			for (unsigned long long c = 0; c < col_to_show_; c++)
			{
				auto mem = this->get_col_ptr(c);
				for (r = 0; r < row_to_show_ - 1; r++)
				{
					os << mem[r] << ",";
				}
				os << mem[r] << "\n";
			}
		}

		void setToZero()
		{
			memset(this->get_memptr(), 0, sizeof(eT) * this->n_elements());
		}

		void fill(eT value)
		{
			auto* ptr = this->get_memptr();
			for (unsigned long long c = 0; c < this->n_elements(); c++)
			{
				ptr[c] = value;
			}
		}


		eT computeAbsSum(unsigned cols, unsigned rows)  const {
			eT res = 0;
			for (unsigned c = 0; c < cols; ++c) {
				for (unsigned r = 0; r < rows; ++r) {
					res += std::abs(this->operator()(r, c));
				}
			}
			return res / (cols * rows);
		}

		static
		std::shared_ptr<ColumnMajorMatrix<eT>> readFromCsv(const std::string& filename, bool skip_header = true)
		{
			std::ifstream file;
			file.open(filename);
			std::vector<std::vector<std::string>> string_matrix;
			std::string line;
			if (skip_header)
			{
				getline(file, line);
			}
			unsigned long n_rows = 0;
			unsigned long n_cols = 0;
			while (getline(file, line)) {
				auto split_r = split(line, ',');
				const auto row_length = split_r.size();
				assert(row_length > 0);
				if (n_cols == 0)
				{
					n_cols = static_cast<unsigned long>(row_length);
				}
				else
				{
					assert(n_cols == row_length);
				}
				string_matrix.push_back(std::move(split_r));
				n_rows++;
			}
			auto matrix = std::make_shared<ColumnMajorMatrix<eT>>(n_rows, n_cols);
			unsigned long row_ctr = 0;
			for (const auto& row : string_matrix)
			{
				for (unsigned long col = 0; col < n_cols; col++)
				{
					eT val;
					if constexpr (std::is_same_v<eT, double>)
					{
						val = std::stod(row[col]);
					}
					else
					{
						val = std::stof(row[col]);
					}
					(*matrix)(row_ctr, col) = val;
				}
				row_ctr++;
			}
			return matrix;
		}

	private:
		unsigned long long computePositionInMemory_(unsigned long long row, unsigned long long col) const
		{
			if (row>=n_rows || col>=n_cols)
			{
				throw std::runtime_error("Out of bounds!");
			}
			return col * this->n_rows + row;
		}
	private:
		std::unique_ptr<eT[]> memory_;

	};

	template<typename eT>
	class ColumnVector : public ColumnMajorMatrix<eT>
	{
	public:
		ColumnVector(unsigned long long n_rows) :
			ColumnMajorMatrix<eT>(n_rows, 1)
		{
		}
	};