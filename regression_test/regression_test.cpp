// regression_test.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <mkl.h>
#include <tuple>
#include <algorithm>
#include <iomanip>

#include "ColumnMajorMatrix.hpp"
#undef MIN
#undef MAX

typedef MKL_INT LAPACK_INT;
typedef MKL_INT BLAS_INT;

template<typename eT>
auto gelss_work(LAPACK_INT m,
	LAPACK_INT n,
	LAPACK_INT nrhs,
	eT* a,
	LAPACK_INT lda,
	eT* b,
	LAPACK_INT ldb,
	eT* s,
	eT rcond,
	LAPACK_INT* rank,
	eT* work,
	LAPACK_INT lwork)
{
	static_assert(std::is_same_v<eT, double> || std::is_same_v<eT, float>, "eT must be either float or double!");
	LAPACK_INT info;
	if constexpr (std::is_same_v<eT, double>) {
		info = LAPACKE_dgelss_work(CblasColMajor, m, n, nrhs, a, lda, b, ldb, s, rcond, rank, work, lwork);
	}
	else
	{
		info = LAPACKE_sgelss_work(CblasColMajor, m, n, nrhs, a, lda, b, ldb, s, rcond, rank, work, lwork);
	}
	return info;
}

template <typename eT>
void gelss_compute_length_s_work(LAPACK_INT m, LAPACK_INT n, LAPACK_INT nrhs, LAPACK_INT* rank, LAPACK_INT& l_s, LAPACK_INT& l_work)
{
	const LAPACK_INT lda = std::max(1, m);
	const LAPACK_INT ldb = std::max(lda, n);
	l_s = std::max(1, std::min(m, n));
	eT wkopt;
	auto info = gelss_work<eT>(m, n, nrhs, nullptr, lda, nullptr, ldb, nullptr, -1.0, rank, &wkopt, -1);
	if (info != 0)
	{
		throw std::runtime_error("_gelss_compute_length_s_work had some problem!");
	}
	l_work = static_cast<LAPACK_INT>(wkopt);
}

template<typename eT>
auto gelss_get_aux_variables(
	LAPACK_INT m,
	LAPACK_INT n,
	LAPACK_INT nrhs,
	LAPACK_INT* rank) {
	LAPACK_INT l_s;
	LAPACK_INT l_work;
	gelss_compute_length_s_work<eT>(m, n, nrhs, rank, l_s, l_work);

	auto s = std::make_unique<eT[]>(l_s);
	auto work = std::make_unique<eT[]>(l_work);
	auto res = std::make_tuple(std::move(s), std::move(work), l_work);
	return res;
}



template<typename eT>
int solve(int niter = 1)
{
	auto const X = ColumnMajorMatrix<eT>::readFromCsv("./ScenarioValues-Fitting.csv");
	auto m = static_cast<LAPACK_INT>(X->n_rows);

	std::vector<std::vector<int>> regressors = { {	0	},
{	1	},
{	2	},
{	3	},
{	4	},
{	5	},
{	6	},
{	7	},
{	8	},
{	9	},
{	10	},
{	11	},
{	12	},
{	13	},
{	14	},
{	15	},
{	16	},
{	17	},
{	18	},
{	19	},
{	20	},
{	21	},
{	22	},
{	23	},
{	24	},
{	26	},
{	27	},
{	29	},
{	31	},
{	32	},
{	33	},
{	37	},
{	39	},
{	27,27	},
{	11,27	},
{	11,11	},
{	11,14	},
{	14,27	},
{	11,11,14	},
{	10,11	},
{	6,6	},
{	11,14,27	},
{	12,14	},
{	10,14,14	},
{	11,12,14	},
{	3,11,27	},
{	14,14	},
{	1,10,11	},
{	10,11,11	},
{	2,6,12	},
{	11,14,14	},
{	10,17,20	},
{	3,3,31	},
{	11,17,20	},
{	1,10,26	},
{	12,14,14	},
{	11,27,27	},
{	8,27,39	},
{	2,11,15	},
{	9,12,20	},
{	27,33,39	},
{	10,10	},
{	11,11,27	},
{	2,10,19	},
{	11,11,11	},
{	10,20,22	},
{	11,21,24	},
{	11,18,19	},
{	29,29,33	},
{	2,11,19	},
{	0,16,26	},
{	0,6,22	},
{	12,20,27	},
{	10,14,27	},
{	18,19,27	},
{	11,11,20	},
{	10,24,27	},
{	2,4,22	},
{	4,18,19	},
{	10,18,37	},
{	6,27	},
{	10,11,14	},
{	1,11,39	},
{	11,15,20	},
{	1,1,2	},
{	16,23,39	},
{	9,13,18	},
{	2,19	},
{	2,14,19	},
{	15,21,23	},
{	9,32,37	},
{	9,31,37	},
{	0,3,27	},
{	17,20,33	},
{	10,11,22	},
{	2,4	},
{	11,24,27	},
{	1,3,17	},
{	11,21,21	},
{	9,27,37	},
{	6,12,31	},
{	5,29,37	},
{	1,1,1	},
{	10,12,32	},
{	13,23,37	},
{	3,11,32	},
{	6,20,32	},
{	17,29,39	},
{	3,13,39	},
{	0,6,11	},
{	3,33,33	},
{	27,29,33	},
{	20,20	},
{	1,24,24	},
{	3,10,32	},
{	3,10,27	},
{	10,19,31	},
{	2,3,6	},
{	4,10,16	},
{	0,18,24	},
{	10,22,33	},
{	4,9,20	},
{	14,19,31	},
{	3,27,33	},
{	3,32,33	},
{	4,5,26	},
{	3,6,29	},
{	21,23,33	},
{	11,18,20	},
{	3,20,39	},
{	11,18,22	},
{	1,3,5	},
{	10,11,31	},
{	2,6,19	},
{	1,18,22	},
{	5,27,37	},
{	1,9,31	},
{	11,27,39	},
{	11,11,23	},
{	9,12,27	},
{	0,6,15	},
{	10,10,19	},
{	2,11,12	},
{	11,16,21	},
{	3,9,32	},
{	11,27,33	},
{	1,24,27	},
{	5,18,21	},
{	6,14	},
{	4,14,23	},
{	11,21,22	},
{	24,27,27	},
{	0,11,16	},
{	2,15,27	},
{	11,27,31	},
{	11,11,32	},
{	1,14,16	},
{	2,3,22	},
{	9,9,37	},
{	1,2,19	},
{	3,11,17	},
{	0,3,39	},
{	2,11,27	},
{	3,4,33	},
{	1,20,24	},
{	11,22,31	},
{	0,11,15	},
{	20,23,32	},
{	8,26,32	},
{	1,1,6	},
{	3,39	},
{	6,6,20	},
{	3,4,20	},
{	0,3,6	},
{	19,29,37	},
{	5,11,11	},
{	6,10,33	},
{	6,17,23	},
{	24,32,37	},
{	7,23,27	},
{	0,24,39	},
{	10,11,32	},
{	8,11,39	},
	};
	std::cout << std::fixed << std::setprecision(16);
	for (int i = 0; i < niter; i++) {
		auto const Y = ColumnMajorMatrix<eT>::readFromCsv("./22Q4_ModelledValues.csv");
		ColumnMajorMatrix<eT> regressor_mat(m, regressors.size());
		regressor_mat.fill(eT(1));
		auto n = static_cast<LAPACK_INT>(regressors.size());
		int col = 0;
		for (const auto& pwrs : regressors) {
			auto* dst = regressor_mat.get_col_ptr(col);
			for (const auto pwr : pwrs) {
				const auto* src = X->get_col_ptr(pwr);
				for (int row = 0; row < m; row++) {
					dst[row] *= src[row];
				}
			}
			col++;
		}
		std::cout << "Y:" << Y->computeAbsSum(1, m) <<"\n";
		std::cout << "Reg:" << regressor_mat.computeAbsSum(n, m) << "\n";

		auto nrhs = 1;
		const auto lda = static_cast<LAPACK_INT>(std::max(1, m));
		const auto ldb = static_cast<LAPACK_INT>(std::max(lda, n));
		eT rcond = -1;
		LAPACK_INT rank;
		auto gelss_tmp_ = gelss_get_aux_variables<eT>(
			m,
			n,
			nrhs,
			&rank);
		auto info = gelss_work<eT>(
			m,
			n,
			nrhs,
			regressor_mat.get_memptr(),
			lda,
			Y->get_memptr(),
			ldb,
			std::get<0>(gelss_tmp_).get(),
			rcond,
			&rank,
			std::get<1>(gelss_tmp_).get(),
			std::get<2>(gelss_tmp_));
		ColumnVector<eT> result(n);
		std::memcpy(result.get_memptr(), Y->get_memptr(), n * sizeof(eT));
		std::cout << result << "\n";
	}
	return 0;
}


int main()
{
	solve<float>(1);
}
